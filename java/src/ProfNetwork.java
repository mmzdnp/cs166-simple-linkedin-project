/*
Group 7
Kashyap Krishna 861065060   kkris001@ucr.edu
Chwan-Hao Tung  861052182   ctung003@ucr.edu
*/


/*
 * Template JAVA User Interface
 * =============================
 *
 * Database Management Systems
 * Department of Computer Science &amp; Engineering
 * University of California - Riverside
 *
 * Target DBMS: 'Postgres'
 *
 */
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * This class defines a simple embedded SQL utility class that is designed to
 * work with PostgreSQL JDBC drivers.
 *
 */
public class ProfNetwork {

   // reference to physical database connection.
   private Connection _connection = null;

   // handling the keyboard inputs through a BufferedReader
   // This variable can be global for convenience.
   static BufferedReader in = new BufferedReader(
                                new InputStreamReader(System.in));

   /**
    * Creates a new instance of Messenger
    *
    * @param hostname the MySQL or PostgreSQL server hostname
    * @param database the name of the database
    * @param username the user name used to login to the database
    * @param password the user login password
    * @throws java.sql.SQLException when failed to make a connection.
    */
   public ProfNetwork (String dbname, String dbport, String user, String passwd) throws SQLException {

      System.out.print("Connecting to database...");
      try{
         // constructs the connection URL
         String url = "jdbc:postgresql://localhost:" + dbport + "/" + dbname;
         System.out.println ("Connection URL: " + url + "\n");

         // obtain a physical connection
         this._connection = DriverManager.getConnection(url, user, passwd);
         System.out.println("Done");
      }catch (Exception e){
         System.err.println("Error - Unable to Connect to Database: " + e.getMessage() );
         System.out.println("Make sure you started postgres on this machine");
         System.exit(-1);
      }//end catch
   }//end ProfNetwork

   /**
    * Method to execute an update SQL statement.  Update SQL instructions
    * includes CREATE, INSERT, UPDATE, DELETE, and DROP.
    *
    * @param sql the input SQL string
    * @throws java.sql.SQLException when update failed
    */
   public void executeUpdate (String sql) throws SQLException {
      // creates a statement object
      Statement stmt = this._connection.createStatement ();

      // issues the update instruction
      stmt.executeUpdate (sql);

      // close the instruction
      stmt.close ();
   }//end executeUpdate

   /**
    * Method to execute an input query SQL instruction (i.e. SELECT).  This
    * method issues the query to the DBMS and outputs the results to
    * standard out.
    *
    * @param query the input query string
    * @return the number of rows returned
    * @throws java.sql.SQLException when failed to execute the query
    */
   public int executeQueryAndPrintResult (String query) throws SQLException {
      // creates a statement object
      Statement stmt = this._connection.createStatement ();

      // issues the query instruction
      ResultSet rs = stmt.executeQuery (query);

      /*
       ** obtains the metadata object for the returned result set.  The metadata
       ** contains row and column info.
       */
      ResultSetMetaData rsmd = rs.getMetaData ();
      int numCol = rsmd.getColumnCount ();
      int rowCount = 0;

      // iterates through the result set and output them to standard out.
      boolean outputHeader = true;
      while (rs.next()){
	 if(outputHeader){
	    for(int i = 1; i <= numCol; i++){
		System.out.print(rsmd.getColumnName(i) + "\t");
	    }
	    System.out.println();
	    outputHeader = false;
	 }
         for (int i=1; i<=numCol; ++i)
            System.out.print (rs.getString (i) + "\t");
         System.out.println ();
         ++rowCount;
      }//end while
      stmt.close ();
      return rowCount;
   }//end executeQuery

   /**
    * Method to execute an input query SQL instruction (i.e. SELECT).  This
    * method issues the query to the DBMS and returns the results as
    * a list of records. Each record in turn is a list of attribute values
    *
    * @param query the input query string
    * @return the query result as a list of records
    * @throws java.sql.SQLException when failed to execute the query
    */
   public List<List<String>> executeQueryAndReturnResult (String query) throws SQLException {
      // creates a statement object
      Statement stmt = this._connection.createStatement ();

      // issues the query instruction
      ResultSet rs = stmt.executeQuery (query);

      /*
       ** obtains the metadata object for the returned result set.  The metadata
       ** contains row and column info.
       */
      ResultSetMetaData rsmd = rs.getMetaData ();
      int numCol = rsmd.getColumnCount ();
      int rowCount = 0;

      // iterates through the result set and saves the data returned by the query.
      boolean outputHeader = false;
      List<List<String>> result  = new ArrayList<List<String>>();
      while (rs.next()){
          List<String> record = new ArrayList<String>();
         for (int i=1; i<=numCol; ++i)
            record.add(rs.getString (i));
         result.add(record);
      }//end while
      stmt.close ();
      return result;
   }//end executeQueryAndReturnResult

   /**
    * Method to execute an input query SQL instruction (i.e. SELECT).  This
    * method issues the query to the DBMS and returns the number of results
    *
    * @param query the input query string
    * @return the number of rows returned
    * @throws java.sql.SQLException when failed to execute the query
    */
   public int executeQuery (String query) throws SQLException {
       // creates a statement object
       Statement stmt = this._connection.createStatement ();

       // issues the query instruction
       ResultSet rs = stmt.executeQuery (query);

       int rowCount = 0;

       // iterates through the result set and count nuber of results.
	   while(rs.next()){
          rowCount++;
       }//end while
       stmt.close ();
       return rowCount;
   }

   /**
    * Method to fetch the last value from sequence. This
    * method issues the query to the DBMS and returns the current
    * value of sequence used for autogenerated keys
    *
    * @param sequence name of the DB sequence
    * @return current value of a sequence
    * @throws java.sql.SQLException when failed to execute the query
    */
   public int getCurrSeqVal(String sequence) throws SQLException {
	Statement stmt = this._connection.createStatement ();

	ResultSet rs = stmt.executeQuery (String.format("Select currval('%s')", sequence));
	if (rs.next())
		return rs.getInt(1);
	return -1;
   }

   /**
    * Method to close the physical connection if it is open.
    */
   public void cleanup(){
      try{
         if (this._connection != null){
            this._connection.close ();
         }//end if
      }catch (SQLException e){
         // ignored.
      }//end try
   }//end cleanup

   /**
    * The main execution method
    *
    * @param args the command line arguments this inclues the <mysql|pgsql> <login file>
    */
   public static void main (String[] args) {
      if (args.length != 3) {
         System.err.println (
            "Usage: " +
            "java [-classpath <classpath>] " +
            ProfNetwork.class.getName () +
            " <dbname> <port> <user>");
         return;
      }//end if

      Greeting();
      ProfNetwork esql = null;
      try{
         // use postgres JDBC driver.
         Class.forName ("org.postgresql.Driver").newInstance ();
         // instantiate the Messenger object and creates a physical
         // connection.
         String dbname = args[0];
         String dbport = args[1];
         String user = args[2];
         esql = new ProfNetwork (dbname, dbport, user, "");

         boolean keepon = true;
         while(keepon) {
            // These are sample SQL statements
            System.out.println("MAIN MENU");
            System.out.println("---------");
            System.out.println("1. Create user");
            System.out.println("2. Log in");
            System.out.println("9. < EXIT");
            String authorisedUser = null;
            switch (readChoice()){
               case 1: CreateUser(esql); break;
               case 2: authorisedUser = LogIn(esql); break;
               case 9: keepon = false; break;
               default : System.out.println("Unrecognized choice!"); break;
            }//end switch
            if (authorisedUser != null) {
              boolean usermenu = true;
              while(usermenu) {
                System.out.println("\nMAIN MENU");
                System.out.println("---------");
                System.out.println("1. Go to Friend List");
                System.out.println("2. Update Profile");
                System.out.println("3. Write a New Message");
                System.out.println("4. Send Friend Request");
                System.out.println("5. View and Delete Messages");
                System.out.println("6. Change Password");
                System.out.println("7. Search for People");
                System.out.println("8. Accept or Reject Connection Requests");
                System.out.println(".........................");
                System.out.println("9. Log out");
                switch (readChoice()){

                  case 1: FriendList(esql,authorisedUser,authorisedUser); break;
                  case 2: UpdateProfile(esql,authorisedUser); break;
                  case 3: NewMessage(esql,authorisedUser,""); break;

                  case 4: SendRequest(esql, authorisedUser, ""); break;

				  case 5: ViewMessage(esql,authorisedUser); break;			   
				  case 6: ChangePw(esql,authorisedUser); break;     
                  case 7: SearchPpl(esql); break;
                  case 8: DealWithRequests(esql,authorisedUser); break;
                  case 9: usermenu = false; break;
                  default : System.out.println("Unrecognized choice!"); break;
                }
              }
            }
         }//end while
      }catch(Exception e) {
         System.err.println (e.getMessage ());
      }finally{
         // make sure to cleanup the created table and close the connection.
         try{
            if(esql != null) {
               System.out.print("Disconnecting from database...");
               esql.cleanup ();
               System.out.println("Done\n\nBye !");
            }//end if
         }catch (Exception e) {
            // ignored.
         }//end try
      }//end try
   }//end main

   public static void Greeting(){
      System.out.println(
         "\n\n*******************************************************\n" +
         "              Group 7 CS166 Project User Interface         \n" +
         "*******************************************************\n");
   }//end Greeting

   /*
    * Reads the users choice given from the keyboard
    * @int
    **/
   public static int readChoice() {
      int input;
      // returns only if a correct value is given.
      do {
         System.out.print("Please make your choice: ");
         try { // read the integer, parse it and break.
            input = Integer.parseInt(in.readLine());
            break;
         }catch (Exception e) {
            System.out.println("Your input is invalid!");
            continue;
         }//end try
      }while (true);
      return input;
   }//end readChoice

   /*
    * Creates a new user with privided login, passowrd and phoneNum
    * An empty block and contact list would be generated and associated with a user
    **/
   public static void CreateUser(ProfNetwork esql){
      try{
         System.out.print("\tEnter user login: ");
         String login = in.readLine();
         System.out.print("\tEnter user password: ");
         String password = in.readLine();
         System.out.print("\tEnter user email: ");
         String email = in.readLine();
		 System.out.print("\tEnter your name (First Last): ");
         String name = in.readLine();
         System.out.print("\tEnter your date of birth (MM/DD/YYYY): ");
         String dob = in.readLine();
         
         DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		 java.sql.Date dateofbirth = new java.sql.Date(format.parse(dob).getTime());
		 
		
		String query = "INSERT INTO USR (userId, password, email, name, dateOfBirth) VALUES ('"+ login +"','"+ password +"','"+ email +"','" + name +"','" + dateofbirth + "')";
		 
/*
		 System.out.println (query);
*/
		 
         esql.executeUpdate(query);
         System.out.println ("User successfully created!");
      }catch(Exception e){
         System.err.println (e.getMessage ());
      }
   }//end

   /*
    * Check log in credentials for an existing user
    * @return User login or null is the user does not exist
    **/
   public static String LogIn(ProfNetwork esql){
      try{
         System.out.print("\tEnter user login: ");
         String login = in.readLine();
         System.out.print("\tEnter user password: ");
         String password = in.readLine();

         String query = String.format("SELECT * FROM USR WHERE userId = '%s' AND password = '%s'", login, password);
         int userNum = esql.executeQuery(query);
		 if (userNum > 0)
		 {
			System.out.println("\n\t\tWelcome to ProfNetwork " + login + "!");
			return login;
		 }
		 else
		 {
			 System.out.println("\nIncorrect login information. Please try again. ");
			 return null;
		 }
      }catch(Exception e){
         System.err.println (e.getMessage ());
         return null;
      }
   }//end
   

	public static void DealWithRequests(ProfNetwork esql, String authorisedUser){
		try
		{
			String query = String.format("SELECT * FROM connection_usr WHERE connectionid = '%s' ", authorisedUser);
			List<List<String>> result =  esql.executeQueryAndReturnResult(query);
			if(result.isEmpty())
			{
				System.out.println("No friend requests to show.");
			}
			for(int i = 0; i < result.size(); i++)
			{
/*
				System.out.println(result.get(i).get(2));
*/
				if(result.get(i).get(2).contains("Request"))
				{
					String req = result.get(i).get(0);
					System.out.println("\nRequest from: " + req);
					System.out.print("\tAccept or Reject? (Enter nothing to return to main menu): ");
					String response = in.readLine();
					if(response.toLowerCase().equals("accept"))
					{
						String update = String.format("UPDATE CONNECTION_USR SET status = 'Accept' WHERE userid = '%s' AND connectionid = '%s'", req, authorisedUser);
						esql.executeUpdate(update);
						System.out.println("Friend request successfully accepted!");	
					}	
					else if(response.toLowerCase().equals("reject"))
					{
						String update = String.format("UPDATE CONNECTION_USR SET status = 'Reject' WHERE userid = '%s' AND connectionid = '%s'", req, authorisedUser);
						esql.executeUpdate(update);
						System.out.println("Friend request rejected!");
					}
					else
					{
						System.out.println("Invalid input! Returning to main menu.");
						return;
					}
				}
			}
			
		}
		catch(Exception e)
		{
			System.err.println (e.getMessage());
		}
	}//end


	//helper function to checkout friend's profile
	public static void CheckProfile(ProfNetwork esql, String authorisedUser ,String friend)
    {
		try
		{
			String query = String.format("SELECT * FROM USR WHERE userId = '%s'", friend);
			List<List<String>> result =  esql.executeQueryAndReturnResult(query);
			String name =  result.get(0).get(3);
			String email = result.get(0).get(2);
			System.out.println("\nName: " + name 
							   + "\nEmail: " + email
							   + "\nDate of Birth: " + result.get(0).get(4));
							   
			System.out.println("\nPlease choose an option");
            System.out.println("---------");
            System.out.println("1. Send this person a message");
            System.out.println("2. Send this person a friend request");
            System.out.println("3. View this person's list of friends");
            System.out.println("4. Exit to main menu");				   
			switch (readChoice()){
               case 1: NewMessage(esql,authorisedUser, friend); break;
               case 2: SendRequest(esql,authorisedUser, friend); break;
			   case 3: FriendList(esql, authorisedUser, friend); break;
			   case 4: break;
               default : System.out.println("Unrecognized choice! Returning to main menu"); break;
            }//end switch
		}
		catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

	public static void FriendList(ProfNetwork esql, String authorisedUser, String target){
		try
		{
			String query = String.format("SELECT * FROM connection_usr WHERE connectionId = '%s' OR userId = '%s'", target, target);
			List<List<String>> result =  esql.executeQueryAndReturnResult(query);
			System.out.println("\n" + target + "'s Friend List: ");
			List<String> friends = new ArrayList<String>();
			for(int i =0; i < result.size(); i++)
			{
				if(result.get(i).get(2).contains("Accept")) //Only friends have their connection status set as accept
				{
					if(result.get(i).get(0).contains(authorisedUser))
					{
						System.out.println("\t" + result.get(i).get(1));
						friends.add(result.get(i).get(1));
					}
					else
					{
						System.out.println("\t" + result.get(i).get(0));
						friends.add(result.get(i).get(0));
					}
					
				}
			}
			if(!friends.isEmpty())
			{
				System.out.println("\nWhose profile would you like to view?(Case sensitive): ");
				String resp = in.readLine();
				
				if(resp.equals(""))
				{
						System.out.println("Returning to menu");
						return;
				}
				
				for(int j = 0; j < friends.size(); j++)
				{
					if(friends.get(j).contains(resp))
					{
						CheckProfile(esql, authorisedUser ,resp);
						return;
					}
				}
				System.out.println("No such friend, returning to main menu.");
			}
			else
			{
				System.out.println(target + " have no friends :'(");
			}
		}
		catch(Exception e)
		{
			System.err.println (e.getMessage());
		}
	}//end
    
    
    //helper function for UpdateProfile
    public static void UpdateName(ProfNetwork esql, String authorisedUser)
    {
		try
		{
			System.out.print("\tWhat's your new name: ");
			String name = in.readLine();
			
			if(name.equals(""))
			{
			  System.out.println("No input, returning to menu");
			  return;
			}
			
			String updateQuery = String.format("UPDATE USR SET name = '%s' WHERE userId = '%s'", name, authorisedUser);
			esql.executeUpdate(updateQuery);
			System.out.println("Name successfully updated!");
		}
		catch(Exception e){
			System.err.println (e.getMessage());
		}
	}
    
    
    public static void SendRequest(ProfNetwork esql, String authorisedUser, String target){
    try{
      boolean canFriend = false;
      String name2friend;
      if(target.equals(""))
      {
		  System.out.println("\nEnter the userid of person to add as a friend: ");
		  name2friend = in.readLine();
		  if(name2friend.equals(""))
		  {
			  System.out.println("No input, returning to menu");
			  return;
		  }
	  }
      else
      {
		  name2friend = target;
	  }
      
      
      String query = String.format("SELECT * FROM connection_usr WHERE userId = '%s' OR connectionId = '%s'", name2friend, name2friend );
      List<List<String>> result =  esql.executeQueryAndReturnResult(query); //get list of target's friends

      List<String> targetList = new ArrayList<String>();
      
      targetList.add(name2friend);
      for(int i =0; i < result.size(); i++){
		if(result.get(i).get(0).contains(name2friend))
		{
			String friend = result.get(i).get(1).replaceAll("'", "''");
			targetList.add(friend);
		}
		else
		{
			String friend = result.get(i).get(0).replaceAll("'", "''");
			targetList.add(friend);
		}
      }

      String query2 = String.format("SELECT * FROM connection_usr WHERE userId = '%s' OR connectionId = '%s'", authorisedUser, authorisedUser);
      List<List<String>> result2 =  esql.executeQueryAndReturnResult(query2); //get list of user's friends

      if (result2.size() < 5) { //can add anyone since user has < 5 friends
        String queryAdd = String.format("INSERT INTO connection_usr (userId, connectionId, status) VALUES ('"+ authorisedUser +"', '"+ name2friend +"', 'Request')");
        esql.executeUpdate(queryAdd);
        System.out.println("Friend request sent!");
        return;
      }

      List<String> list2check = new ArrayList<String>();
      
      for (int i = 0; i < result2.size(); i++){             //populating list2check with user's friends and user's friends' friends
		
		String jump1 = "";
		
		if(result2.get(i).get(0).contains(authorisedUser))
		{
			jump1 = result2.get(i).get(1);
		}
		else
		{
			jump1 = result2.get(i).get(0);
		}
		
		jump1 = jump1.replaceAll("'", "''");
		
        list2check.add(jump1);
        String query3 = String.format("SELECT * FROM connection_usr WHERE userId = '%s' OR connectionId = '%s'", jump1,jump1);
        List<List<String>> result3 =  esql.executeQueryAndReturnResult(query3); //get list of friend's friends
		

		
        for (int j = 0; j < result3.size(); j++){
			if(result3.get(j).get(0).contains(jump1))
			{
				String tmp = result3.get(j).get(1).replaceAll("'", "''");
				list2check.add(tmp);
			}
			else
			{
				String tmp = result3.get(j).get(0).replaceAll("'", "''");
				list2check.add(tmp);
			}	
        }
      }

      for (int i = 0; i < targetList.size(); i++){
        for (int j = 0; j < list2check.size(); j++){
          if (targetList.get(i).equals(list2check.get(j))){
            canFriend = true;
          }
        }
      }
	  
	 
	  
      if (canFriend){
        //send the friend request
        String queryAdd = String.format("INSERT INTO connection_usr (userId, connectionId, status) VALUES ('"+ authorisedUser +"', '"+ name2friend +"', 'Request')");
        esql.executeUpdate(queryAdd);
        
        
        
        System.out.println("Friend request sent!");
      }
      else
      {
		  System.out.println("Sorry, you're not connected enough to add this person as friend!");
	  }

    }
    catch(Exception e)
    {
      System.err.println (e.getMessage ());
    }
  }
    
    
    
    //helper function for UpdateProfile
    public static void UpdateEmail(ProfNetwork esql, String authorisedUser)
    {
		try
		{
			System.out.print("\tWhat's your new email: ");
			String email = in.readLine();
			
			if(email.equals(""))
			{
			  System.out.println("No input, returning to menu");
			  return;
			}
			
			String updateQuery = String.format("UPDATE USR SET email = '%s' WHERE userId = '%s'", email, authorisedUser);
			esql.executeUpdate(updateQuery);
			System.out.println("Email successfully updated!");
		}
		catch(Exception e){
			System.err.println (e.getMessage());
		}
	}
    
    //helper function for UpdateProfile
    public static void UpdateDob(ProfNetwork esql, String authorisedUser)
    {
		try
		{
			System.out.print("\tWhat is your date of birth (MM/DD/YYYY): ");
			String dob = in.readLine();
            
            if(dob.equals(""))
			{
			  System.out.println("No input, returning to menu");
			  return;
			}
            
            
			DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			java.sql.Date dateofbirth = new java.sql.Date(format.parse(dob).getTime());
			
			String updateQuery = "UPDATE USR SET dateOfBirth = '" + dateofbirth + "' WHERE userId = '" + authorisedUser + "'";
			esql.executeUpdate(updateQuery);
			System.out.println("Date of birth successfully updated!");
			
		}
		catch(Exception e){
			System.err.println (e.getMessage());
		}
	}
    
   //updates profile
	public static void UpdateProfile(ProfNetwork esql, String authorisedUser){
		try
		{
			System.out.println("What would you like to update?");
            System.out.println("---------");
            System.out.println("1. Name");
            System.out.println("2. Email");
            System.out.println("3. Date of Birth");
            System.out.println("4. Exit to main menu");
            switch (readChoice()){
               case 1: UpdateName(esql,authorisedUser); break;
               case 2: UpdateEmail(esql,authorisedUser); break;
			   case 3: UpdateDob(esql,authorisedUser); break;
			   case 4: break;
               default : System.out.println("Unrecognized choice! Returning to main menu"); break;
            }//end switch
		}
		catch(Exception e){
			System.err.println (e.getMessage());
		}
	}//end
   
   
   
public static void ViewRecieved(ProfNetwork esql, String authorisedUser)
{
	try
	{
		   
		 String query = String.format("SELECT * FROM MESSAGE WHERE receiverId = '%s'", authorisedUser);
		 List<List<String>> result =  esql.executeQueryAndReturnResult(query);
		 
		 System.out.println("Here is a list of your received messages");
		 
		 
		 for(int i =0; i < result.size(); i++)
		 {
			 //"field 5 is delete status, if its not deleted, 2 meaning sender cant see but reciever can see
			if(result.get(i).get(5).equals("0") || result.get(i).get(5).equals("2")) 
			{
				System.out.println("\nMessage ID: " + result.get(i).get(0) + "\n"
									+ "From: " + result.get(i).get(1) + "\n\t"
									+ result.get(i).get(4) + "\n" 
									+ result.get(i).get(3));
			}
		 }
		 System.out.print("Would you like to delete a message? (Y/N): ");
		 String response= in.readLine();
		 if(response.toLowerCase().equals("y") || response.toLowerCase().equals("yes"))
		 {
			 System.out.print("Enter the Message ID of the message you would like to delete: ");
			 String inp = in.readLine();
			 int deleteStatus = 0;
			 boolean present = false;
			 for(int i =0; i < result.size(); i++)
			 {
				 if(result.get(i).get(0).equals(inp))
				 {
					present = true;
					deleteStatus = Integer.parseInt(result.get(i).get(5));
					break;
				}
			 }			
			 if(present)
			 {
				 int mid = Integer.parseInt(inp);
				  // delete the message using UPDATE, 1 meaning receiver cant see, sender can see, if its 2 already, set to 3
				 if(deleteStatus == 2)
				 {
					 deleteStatus = 3;
				 }
				 else
				 {
					 deleteStatus =1;
				 }
				 String updateQuery = String.format("UPDATE MESSAGE SET deleteStatus = '%d' WHERE receiverId = '%s' AND msgId = '%d'", deleteStatus ,authorisedUser, mid);
				 esql.executeUpdate(updateQuery);
				 System.out.println("Message deleted");
			 }
			 else
			 {
				 System.out.print("\nNo matching messages, Please try again\n");
				 ViewMessage(esql, authorisedUser);
			 }
		}
	}catch(Exception e){
			System.err.println (e.getMessage ());
	}
}

public static void ViewSent(ProfNetwork esql, String authorisedUser){
	try
	{
		String query = String.format("SELECT * FROM MESSAGE WHERE senderId = '%s'", authorisedUser);
		List<List<String>> result =  esql.executeQueryAndReturnResult(query);
		 
		System.out.println("Here is a list of your sent messages");
		 
		 
		for(int i =0; i < result.size(); i++)
		{
			//"field 5 is delete status, if its not deleted, 1 meaning receiver cant see but sender can see
			if(result.get(i).get(5).equals("0") || result.get(i).get(5).equals("1")) 
			{
				System.out.println("\nMessage ID: " + result.get(i).get(0) + "\n"
									+ "To: " + result.get(i).get(2) + "\n\t"
									+ result.get(i).get(4) + "\n" 
									+ result.get(i).get(3));
			}
		}
		System.out.print("Would you like to delete a message? (Y/N): ");
		String response= in.readLine();
		if(response.toLowerCase().equals("y") || response.toLowerCase().equals("yes"))
		{
			System.out.print("Enter the Message ID of the message you would like to delete: ");
			String inp = in.readLine();
			int deleteStatus = 0;
			boolean present = false;
			for(int i =0; i < result.size(); i++)
			{
				if(result.get(i).get(0).equals(inp))
				{
					present = true;
					deleteStatus = Integer.parseInt(result.get(i).get(5));
					break;
				}
			}			
			if(present)
			{
				int mid = Integer.parseInt(inp);
				// delete the message using UPDATE, 2 meaning sender cant see, rec can see, if its 1 already, set to 3
				if(deleteStatus == 1)
				{
					deleteStatus = 3;
				}
				else
				{
					deleteStatus = 2;
				}
				String updateQuery = String.format("UPDATE MESSAGE SET deleteStatus = '%d' WHERE senderId = '%s' AND msgId = '%d'", deleteStatus ,authorisedUser, mid);
				esql.executeUpdate(updateQuery);
				System.out.println("Message deleted");
			}
			else
			{
				System.out.print("\nNo matching messages, Please try again\n");
				ViewMessage(esql, authorisedUser);
			}
		}	
	}
	catch(Exception e){
         System.err.println (e.getMessage());
	}
}


   public static void ViewMessage(ProfNetwork esql, String authorisedUser){
      try{
		  
			System.out.println("Please choose an option");
			System.out.println("---------");
			System.out.println("1. View received");
			System.out.println("2. View sent");
			System.out.println("3. Exit to main menu");
            switch (readChoice()){
               case 1: ViewRecieved(esql,authorisedUser); break;
               case 2: ViewSent(esql,authorisedUser); break;
			   case 3: break;
               default : System.out.println("Unrecognized choice! Returning to main menu"); break;
            }//end switch
		  
		  
	  }	  
      catch(Exception e){
         System.err.println (e.getMessage());
      }
   }//end
   
   
	public static void ChangePw(ProfNetwork esql, String authorisedUser){
		try{
         System.out.print("\tEnter your current password: ");
         String password = in.readLine();
         String query = String.format("SELECT * FROM USR WHERE userId = '%s' AND password = '%s'", authorisedUser, password);
         int userNum = esql.executeQuery(query);
		 if (userNum > 0)
		 {
			 System.out.print("\tEnter new password: ");
			 String newPw = in.readLine();
			 String updateQuery = String.format("UPDATE USR SET password = '%s' WHERE userId = '%s'", newPw, authorisedUser);
			 esql.executeUpdate(updateQuery);
			 System.out.println("Password successfully updated!");
			 
		 }
         else
         {
			 System.out.println("\nIncorrect current password. Please try again."); 
		 }
		}catch(Exception e){
			System.err.println (e.getMessage ());
		}

   }
   
   
   public static void NewMessage(ProfNetwork esql,String authorisedUser, String destination){
      try{
		 String recipient;
		 if(destination.equals("")) //if destination isnt specified, ask for it
		 { 
			System.out.print("\tWho are you sending this message to?: ");
			recipient = in.readLine();
			
			if(recipient.equals(""))
			{
			  System.out.println("No input, returning to menu");
			  return;
			}
			
		 }
		 else
		 {
			 recipient = destination;
		 }
		 
         String checkquery = String.format("SELECT * FROM USR WHERE userId = '%s'", recipient);
         int userNum = esql.executeQuery(checkquery);
         
         if(userNum == 0)
         {
			 System.out.println("\nNo such user. Please try again");
			 return;
		 }
         
         System.out.print("\tEnter your message: ");
         String contents = in.readLine();
         
         java.util.Date today = new java.util.Date();
		 Timestamp sendtime = new java.sql.Timestamp(today.getTime());
		 
		 String rowQuery = String.format("SELECT * FROM MESSAGE");
		 int numMsgs = esql.executeQuery(rowQuery);
		 int msgId = numMsgs +1;

		 String query = "INSERT INTO MESSAGE (msgId, senderId, receiverid, contents, sendTime, deleteStatus,status) VALUES ('" + msgId + "','" + authorisedUser + "','" + recipient + "','" + contents + "','" + sendtime + "','0','Delivered')";

         esql.executeUpdate(query);
         
         System.out.println("Message sent successfully!");
         
      }catch(Exception e){
         System.err.println (e.getMessage ());
      }
   }//end


	public static void SearchPpl(ProfNetwork esql){
		try
		{
			System.out.println("Enter name of person to find: ");
			String name2find = in.readLine();
			
			if(name2find.equals(""))
			{
			  System.out.println("No input, returning to menu");
			  return;
			}
			
			String query = String.format("SELECT \"userid\", \"name\" FROM usr WHERE name = '%s' ", name2find);
			List<List<String>> result =  esql.executeQueryAndReturnResult(query);
			
			if(result.isEmpty())
			{
				System.out.println("Person not found" );
			}
			
			
			for(int i =0; i < result.size(); i++)
			{
			  
				System.out.println("Name: " + result.get(i).get(1) 
									+ "\n" + "userid: " + result.get(i).get(0) + "\n" );
			}
		}
		catch(Exception e)
		{
			System.err.println (e.getMessage ());
		}
  }
    
   
// Rest of the functions definition go in here

}//end ProfNetwork
