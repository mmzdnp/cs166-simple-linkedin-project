/*
Group 7
Kashyap Krishna 861065060   kkris001@ucr.edu
Chwan-Hao Tung  861052182   ctung003@ucr.edu
*/


CREATE UNIQUE INDEX USR_index 
ON USR(userId);


CREATE UNIQUE INDEX WORK_index
ON WORK_EXPR(userId,company,role,startDate);


CREATE UNIQUE INDEX EDU_index
ON EDUCATIONAL_DETAILS(userId,major,degree);


CREATE UNIQUE INDEX MSG_index
ON MESSAGE(msgId);

CREATE UNIQUE INDEX CONN_index
ON CONNECTION_USR(userId,connectionId);
