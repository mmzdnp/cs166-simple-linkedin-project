/*
Group 7
Kashyap Krishna 861065060   kkris001@ucr.edu
Chwan-Hao Tung  861052182   ctung003@ucr.edu
*/

COPY USR FROM '/class/classes/ctung003/cs166/project/CS166_Project/data/USR_Data.csv' WITH CSV HEADER;

COPY WORK_EXPR FROM '/class/classes/ctung003/cs166/project/CS166_Project/data/work_data.csv' WITH CSV HEADER;

COPY EDUCATIONAL_DETAILS FROM '/class/classes/ctung003/cs166/project/CS166_Project/data/edu_data.csv' WITH CSV HEADER;

COPY MESSAGE FROM '/class/classes/ctung003/cs166/project/CS166_Project/data/msg_data.csv' WITH CSV HEADER;

COPY CONNECTION_USR FROM '/class/classes/ctung003/cs166/project/CS166_Project/data/conn_data.csv' WITH CSV HEADER;
